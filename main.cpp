#include <array>
#include <iostream>

int main()
{
    std::cout.sync_with_stdio(false);

    std::cout << "# Unsigned arithmetic" << '\n';
    unsigned int a { 1 };
    unsigned int b { 2 };
    unsigned int c { a - b };
    std::cout << c << '\n';

    std::cout << "# Unsigned definition" << '\n';
    // unsigned int e { -1 };
    unsigned int e = -1;
    std::cout << e << '\n';

    std::cout << "# Int arithmetic" << '\n';
    int f { 2147483647 };
    int g { 1 };
    int h { f + g };
    std::cout << h << '\n';

    std::cout << "# Int definition" << '\n';
    // int d { 2147483648 };
    int d = 2147483648;
    std::cout << d << '\n';

    std::cout << "# C arrays" << '\n';
    int arr[1] { 1 };
    std::cout << arr[1] << "\n";
    std::cout << arr[-1] << "\n";

    std::cout << "# C++ arrays" << '\n';
    std::array<int, 1> arrpp { 1 };
    std::cout << arrpp[1] << "\n";
    std::cout << arrpp[-1] << "\n";

    return 0;
}
