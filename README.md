# cpp-overflow

Compile with `make`.
Default compiler is `gcc`.
Change to `clang` with
```shell
export CXX=clang++
```
Run with `./main`.
