CXX?=g++

.PHONY: clean

main: main.o
	$(CXX) $< -o $@

main.o: main.cpp
	$(CXX) -c $< -o $@ -Wall -Wextra

clean:
	rm -f main main.o
